package com.desafio.digitounico.controller;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import com.desafio.digitounico.controllers.UsuarioController;
import com.desafio.digitounico.dtos.UsuarioDto;
import com.desafio.digitounico.entities.Usuario;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(UsuarioController.class)
@RunWith(SpringRunner.class)
public class UsuarioControllerTest {

    private static final Long ID = 1L;
    private static final String NOME = "Josesmar";
    private static final String SOBRENOME = "Santos";
    private static final String EMAIL = "josesmargds@gmail.com";
    private static final String URL = "/api/usuario";
    private static final ObjectMapper OM = new ObjectMapper();

    @MockBean
    private UsuarioController usuarioController;

    @Autowired
    private MockMvc mvc;


    @Test
    public void getAll() throws Exception {
        UsuarioDto dto = UsuarioDto.builder().id(ID).nome(NOME).email(EMAIL).build();
        List<UsuarioDto> dtos = Arrays.asList(dto);

        given(usuarioController.listUsers()).willReturn((ResponseEntity<List<Usuario>>) dtos);

        mvc.perform(get(URL + "/all")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

}
