package com.desafio.digitounico.exceptions;

import lombok.Data;

import java.io.Serializable;
import java.time.Instant;

/**
 * @autor Josesmar Santos 14/09/2021
 */

@Data
public class StandarError implements Serializable {

    private Instant timestamp;
    private Integer status;
    private String error;
    private String message;
    private String path;

    public StandarError() {

    }

    public Instant getTimestamp() {
        return timestamp;
    }

    public Integer getStatus() {
        return status;
    }

    public String getError() {
        return error;
    }

    public String getMessage() {
        return message;
    }

    public String getPath() {
        return path;
    }

    public StandarError(String defaultMessage, String field, Object rejectedValue) {
    }
}