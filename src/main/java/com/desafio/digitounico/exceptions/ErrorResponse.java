package com.desafio.digitounico.exceptions;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.Date;
import java.util.List;

/**
 * @autor Josesmar Santos 14/09/2021
 */

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ErrorResponse {

    private String message;
    private Date currentDate;
    private int code;
    private String status;
    private String objectName;
    private List<StandarError> errors;


    @ResponseStatus(HttpStatus.NOT_FOUND)
    public static class EntityNotFoundException extends RuntimeException{
        private static final long serialVersionUID = 1L;

        public EntityNotFoundException(String message){
            super(message);
        }

    }

    public ErrorResponse(Date currentDate, String message){
        this.currentDate = currentDate;
        this.message = message;
    }
}