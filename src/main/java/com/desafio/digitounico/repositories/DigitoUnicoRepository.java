package com.desafio.digitounico.repositories;

import com.desafio.digitounico.entities.DigitoUnico;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

/**
 * @author Josesmar Santos 14/09/2021
 * */


@Repository
public interface DigitoUnicoRepository extends JpaRepository<DigitoUnico, Long> {

    @Query("select distinct diu.id FROM DigitoUnico diu where diu.idUsuario = (:idUsuario)")
    public List<DigitoUnico> getAllByUsuario(@Param("idUsuario") Long idUsuario);

    @Query("select distinct count(diu.id) FROM DigitoUnico diu where diu.digitoGerado = (:digitoGerado)")
    public int validarDigito(@Param("digitoGerado") Integer digitoGerado);
}
