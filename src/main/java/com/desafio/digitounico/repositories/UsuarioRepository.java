package com.desafio.digitounico.repositories;

import com.desafio.digitounico.entities.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import java.util.List;
import java.util.Optional;

/**
 * @author Josesmar Santos 14/09/2021
 * */


public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

    @Override
    @Query("SELECT DISTINCT u FROM Usuario u LEFT JOIN FETCH u.digitos")
    public List<Usuario> findAll();

    @Override
    @Query("SELECT DISTINCT u FROM Usuario u LEFT JOIN FETCH u.digitos WHERE u.id = (:id)")
    public Optional<Usuario> findById(@Param("id") Long id);

    @Query("SELECT DISTINCT COUNT(u.id) FROM Usuario u WHERE u.id = (:id)")
    public Integer validarUsuario(@Param("id") Long id);
}
