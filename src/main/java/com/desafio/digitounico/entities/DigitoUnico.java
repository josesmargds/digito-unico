package com.desafio.digitounico.entities;

import com.sun.istack.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Persistable;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author: Josesmar Santos 13/09/2021
 * */

@Entity
@Table(name="tb_digitoUnico")
@Data
@NoArgsConstructor
public class DigitoUnico implements Serializable, Persistable<Long> {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    public Long id;

    @Column
    public String digitoParametro;

    @Column
    public Integer multiplicador;

    @Column
    public String digitoGerado;

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    public Usuario idUsuario;

    public DigitoUnico(String digitoParametro, Integer multiplicador, String digitoGerado) {
        this.digitoParametro = digitoParametro;
        this.multiplicador = multiplicador;
        this.digitoGerado = digitoGerado;
    }

    public Usuario getUsuario() {
        return idUsuario == null ? new Usuario() : idUsuario;
    }

    @Override
    public boolean isNew() {
        return false;
    }
}

