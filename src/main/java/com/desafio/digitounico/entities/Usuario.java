package com.desafio.digitounico.entities;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Persistable;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author: Josesmar Santos 13/09/2021
 * */


@Entity
@Table(name="tb_usuario")
@Data
@NoArgsConstructor
public class Usuario implements Serializable, Persistable<Long> {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @Column
    private String nome;

    @Column
    private String email;

    @OneToMany(mappedBy="idUsuario", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @Column
    private Set<DigitoUnico> digitos = new HashSet<>();

    public Usuario(Long id) {
        this.id = id;
    }

    @Override
    public boolean isNew() {
        return false;
    }
}

