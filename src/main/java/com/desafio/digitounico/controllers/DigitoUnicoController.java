package com.desafio.digitounico.controllers;

import java.util.List;

import com.desafio.digitounico.dtos.ParametroDigitoDTO;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.desafio.digitounico.entities.DigitoUnico;
import com.desafio.digitounico.services.DigitoUnicoService;
import com.desafio.digitounico.dtos.DigitoUnicoDto;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import javax.validation.Valid;

/**
 * @author Josesmar Santos 14/09/2021
 * */

@RestController
@Slf4j
@RequestMapping("/api/digitounico")
public abstract class DigitoUnicoController extends AbstractController<DigitoUnico, DigitoUnicoDto, Long> {

    @Autowired
    DigitoUnicoService digitoUnicoService;

    @GetMapping("/all/usuario/{id}")
    @ResponseStatus(code = HttpStatus.OK)
    @ApiOperation(httpMethod = "GET",
            value = "Lista todos os dígitos de um usuário específico",
            nickname = "getAllByUsuario",
            tags = { "digitounico", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Lista de dígitos"),
            @ApiResponse(code = 204, message = "Nenhum dígito encontrado!"),
            @ApiResponse(code = 400, message = "Erro!")})
    public List<DigitoUnicoDto> getAllByUsuario (@PathVariable(value = "id") Long id) {
        log.debug(" >> findAllByUsuario [id={}] ", id);
        digitoUnicoService.validarUsuario(id);
        List<DigitoUnicoDto> list = digitoUnicoService.getAllByUsuario(id);
        log.debug(" << findAllByUsuario [id={}] ", id);
        return list;
    }

    @PostMapping("/calcular")
    @ApiOperation(httpMethod = "POST",
            value = "Criar um dígito",
            nickname = "createDigito",
            tags = { "digitounico", })
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Dígito criado com sucesso"),
            @ApiResponse(code = 400, message = "Falha ao criar o dígito")})
    public ResponseEntity<DigitoUnicoDto> createDigito(@Valid @RequestBody ParametroDigitoDTO dto) {
        log.debug(" >> createDigito [dto={}] ", dto);
        Integer digitoUnico = digitoUnicoService.calcularDigitoUnico(dto);
        DigitoUnicoDto dtoCriado = digitoUnicoService.createDigito(dto, digitoUnico);
        log.debug(" << createDigito [dto={}] ", dto);
        return new ResponseEntity<>(dtoCriado, HttpStatus.CREATED);
    }

    @PostMapping("/calcular/usuario")
    @ApiOperation(httpMethod = "POST",
            value = "Criar um dígito para um usuário específico",
            nickname = "createDigitoByUsuario",
            tags = { "digitounico", })
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Dígito criado com sucesso"),
            @ApiResponse(code = 400, message = "Falha ao criar o dígito")})
    public ResponseEntity<DigitoUnicoDto> createDigitoByUsuario(@Valid @RequestBody ParametroDigitoDTO dto) {
        log.debug(" >> createDigitoByUsuario [dto={}] ", dto);
        digitoUnicoService.validarUsuario(dto.getIdUsuario());
        Integer digitoUnico = digitoUnicoService.calcularDigitoUnico(dto);
        DigitoUnicoDto dtoCriado = digitoUnicoService.createDigito(dto, digitoUnico);
        log.debug(" << createDigitoByUsuario [dto={}] ", dto);
        return new ResponseEntity<>(dtoCriado, HttpStatus.CREATED);
    }

}
