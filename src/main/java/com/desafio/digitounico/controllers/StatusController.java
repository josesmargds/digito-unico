package com.desafio.digitounico.controllers;

import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Josesmar Santos 15/09/2021
 * */

@RestController
public class StatusController {

    @GetMapping(path = "/api/status")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(httpMethod = "GET",
            value = "Verifica status da API",
            nickname = "CheckStatus",
            tags = {"Status"}
    )
    public String check(){
        return "Online";}
}
