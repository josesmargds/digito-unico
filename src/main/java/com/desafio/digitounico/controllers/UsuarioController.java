package com.desafio.digitounico.controllers;

import com.desafio.digitounico.dtos.response.MessageResponseDto;
import com.desafio.digitounico.entities.Usuario;
import com.desafio.digitounico.exceptions.ResourceNotFoundException;
import com.desafio.digitounico.services.UsuarioService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author Josesmar Santos 14/09/2021
 * */

@RestController
@RequestMapping("/api/usuario")
@CrossOrigin(origins = "*")
@AllArgsConstructor(access = AccessLevel.PROTECTED)
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @GetMapping(value = "/all")
    @ApiOperation(httpMethod = "GET",
            value = "Retorna a lista de usuários",
            nickname = "createDigito",
            tags = { "usuario", })
    public ResponseEntity<List<Usuario>> listUsers() {
        List<Usuario> list = usuarioService.findAll();
        return ResponseEntity.ok().body(list);
    }

    @GetMapping(value = "/{id}")
    @ApiOperation(httpMethod = "GET",
            value = "Retorna um usuário",
            nickname = "getUserById",
            tags = { "usuario", })
    public ResponseEntity<Usuario> findbyId(@PathVariable Long id){
        Usuario usuarioObj = usuarioService.findbyId(id);
        return ResponseEntity.ok().body(usuarioObj);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED) //Retorna o código 201
    @ApiOperation(httpMethod = "POST",
            value = "Cria uma novo usuário",
            nickname = "createUser",
            tags = { "usuario", })
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Usuário criado com sucesso"),
            @ApiResponse(code = 400, message = "Falha ao criar usuário")})
    public MessageResponseDto insert(@RequestBody Usuario usuario){
        return usuarioService.save(usuario);
    }

    @DeleteMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ApiOperation(httpMethod = "DELETE",
            value = "Exclui um usuario",
            nickname = "deleteUser",
            tags = { "usuario", })
    public MessageResponseDto delete(@PathVariable Long id){
        return usuarioService.delete(id);
    }


    @PutMapping(path = "/{id}")
    @ApiOperation(httpMethod = "PUT",
            value = "Atualiza um usuário\"",
            nickname = "updateUser",
            tags = { "usuario", })
    public MessageResponseDto updateById(@PathVariable Long id, @RequestBody @Valid Usuario usuario) throws ResourceNotFoundException {
        return usuarioService.updateById(id, usuario);
    }
}
