package com.desafio.digitounico;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan({ "com.desafio.digitounico" })
@SpringBootApplication
public class DigitounicoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DigitounicoApplication.class, args);
	}

}
