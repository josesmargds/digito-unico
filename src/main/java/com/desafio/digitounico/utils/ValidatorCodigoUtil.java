package com.desafio.digitounico.utils;

import com.desafio.digitounico.dtos.ParametroDigitoDTO;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.ResponseEntity;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ValidatorCodigoUtil {
    public static ResponseEntity<String> validarParametros(ParametroDigitoDTO dto) {
        List<String> erros = listarErrosParam(dto);
        if (CollectionUtils.isNotEmpty(erros)) {
            String body = "";
            erros.forEach(body::concat);
            return ResponseEntity.badRequest().body(body);
        }

        return ResponseEntity.ok().body("OK");
    }

    public static List<String> listarErrosParam(ParametroDigitoDTO dto) {
        List<String> list = new ArrayList<>();

        if (Objects.isNull(dto.getDigitoParametro())) {
            list.add("O dígito não pode ser nulo!");
        }

        if (!StringUtils.isNumeric(dto.getDigitoParametro())) {
            list.add("O dígito informado contém caracteres não numéricos");
        }

        BigInteger concatValorMaximo = BigInteger.TEN.pow(5);
        if (Objects.nonNull(dto.getConcatenacao()) && (dto.getConcatenacao() < 1
                || (concatValorMaximo.compareTo(BigInteger.valueOf(dto.getConcatenacao())) < 0))) {
            list.add("Concatenação excede o máximo(10^5).");
        }

        BigInteger tamanhoMax = BigInteger.TEN.pow(1000000);
        if ((new BigInteger(dto.getDigitoParametro()).compareTo(BigInteger.ONE) < 0)
                || (tamanhoMax.compareTo(new BigInteger(dto.getDigitoParametro())) < 0)) {
            list.add("O dígito informado excede o máximo(10^^1000000).");
        }

        return list;
    }

    public static ResponseEntity<String> validarCriptografiaUsuario(Long id, boolean isCriptografia) {
        List<String> erros = listarErrosCriptografia(id, isCriptografia);
        if (CollectionUtils.isNotEmpty(erros)) {
            String body = "";
            for (String string : erros) {
                body = body.concat(" ").concat(string);
            }
            return ResponseEntity.badRequest().body(body);
        }

        return ResponseEntity.ok().body("OK");
    }

    private static List<String> listarErrosCriptografia(Long id, boolean isCriptografia) {
        List<String> list = new ArrayList<>();
        if (Objects.isNull(id)) {
            list.add("O id usuário não pode ser nulo!");
        }

//        if ((isCriptografia && CriptografiaUtils.usuarioPossuiChave(id))) {
//            list.add("O usuário já foi criptografado!");
//        }
//
//        if ((!isCriptografia && !CriptografiaUtils.usuarioPossuiChave(id))) {
//            list.add("O usuário ainda não foi criptografado!");
//        }
        return list;
    }
}
