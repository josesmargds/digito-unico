package com.desafio.digitounico.utils;

import java.util.Objects;

import com.desafio.digitounico.dtos.DigitoUnicoDto;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.databind.util.Converter;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.desafio.digitounico.entities.DigitoUnico;
import com.desafio.digitounico.entities.Usuario;
import com.desafio.digitounico.repositories.UsuarioRepository;

/**
 * @author: Josesmar Santos 14/09/2021
 * */

@Component
public class DigitoUnicoConverterUtil implements Converter<DigitoUnico, DigitoUnicoDto> {

    @Autowired
    UsuarioRepository usuarioRepository;

    public DigitoUnico convertToEntity(DigitoUnicoDto dto) {
        DigitoUnico entity = new DigitoUnico();
        BeanUtils.copyProperties(dto, entity);
        if (Objects.nonNull(dto.getIdUsuario())) {
            Usuario usuario = usuarioRepository.getOne(dto.getIdUsuario());
            entity.setIdUsuario(usuario);
        }
        return entity;
    }
    public DigitoUnicoDto convertToDTO(DigitoUnico entity) {
        DigitoUnicoDto dto = new DigitoUnicoDto();
        BeanUtils.copyProperties(entity, dto);
        if (Objects.nonNull(entity.getUsuario())) {
            dto.setIdUsuario(entity.getUsuario().getId());
        }
        return dto;
    }

    @Override
    public DigitoUnicoDto convert(DigitoUnico digitoUnico) {
        return null;
    }

    @Override
    public JavaType getInputType(TypeFactory typeFactory) {
        return null;
    }

    @Override
    public JavaType getOutputType(TypeFactory typeFactory) {
        return null;
    }
}
