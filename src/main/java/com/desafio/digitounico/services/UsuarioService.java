package com.desafio.digitounico.services;
import java.util.List;
import java.util.Objects;
import com.desafio.digitounico.dtos.response.MessageResponseDto;
import com.desafio.digitounico.exceptions.ErrorResponse;
import com.desafio.digitounico.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.desafio.digitounico.entities.Usuario;
import com.desafio.digitounico.repositories.UsuarioRepository;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Josesmar Santos 14/09/2021
 * */


@Slf4j
@Service
@Transactional(propagation = Propagation.REQUIRED)
public class UsuarioService{

    @Autowired
    private UsuarioRepository usuarioRepository;

    public Usuario findbyId(Long id) throws ResourceNotFoundException {
        verifyIfUserExistis(id);
        return usuarioRepository.findById(id).
                orElseThrow(() -> new ErrorResponse.EntityNotFoundException("Não existe usuário com id: " + id));
    }

    public MessageResponseDto save(Usuario usuario){
        usuarioRepository.save(usuario);
        Usuario salvarUsuario = usuarioRepository.save(usuario);
        return createMessageResponse(salvarUsuario.getId(), "Usuário " + salvarUsuario.getNome() +  " registrado com sucesso com o id: ");
    }

    public List<Usuario> findAll(){
        return usuarioRepository.findAll();
    }

    public MessageResponseDto delete(Long id) throws ResourceNotFoundException{
        verifyIfUserExistis(id);
        usuarioRepository.deleteById(id);
        return createMessageResponse(id, "Usuário excluido com sucesso!.");
    }

    public MessageResponseDto updateById(Long id, Usuario usuario) throws ResourceNotFoundException {
        verifyIfUserExistis(id);
        Usuario updatedUsuario = usuarioRepository.save(usuario);
        return createMessageResponse(updatedUsuario.getId(), "Atualizado usuário com id: ");
    }

    public boolean validarUsuario(Long id) {
        if (Objects.isNull(id)) {
            return Boolean.FALSE;
        }
        log.debug(">> validarUsuario [id={}] ", id);
        boolean result = usuarioRepository.validarUsuario(id) > 0;
        log.debug(">> validarUsuario [id={}] ", id);
        return result;
    }

    /**
     * Criando mensagem padrão
     * @param id
     * @param message
     * @return
     */
    private MessageResponseDto createMessageResponse(Long id, String message) {
        verifyIfUserExistis(id);
        return MessageResponseDto
                .builder()
                .message(message + id)
                .build();
    }

    /**
     * Valida se existe usuário
     * @param id
     */
    private void verifyIfUserExistis(Long id){
        if (usuarioRepository.findById(id) == null){
            throw new ResourceNotFoundException("Usuário não existe com id: " + id);
        }
    }

}
