package com.desafio.digitounico.services;

import com.desafio.digitounico.dtos.DigitoUnicoDto;
import com.desafio.digitounico.dtos.ParametroDigitoDTO;
import com.desafio.digitounico.entities.DigitoUnico;
import com.desafio.digitounico.repositories.DigitoUnicoRepository;
import com.desafio.digitounico.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author Josesmar Santos 14/09/2021
 * */


@Slf4j
@Transactional(propagation=Propagation.REQUIRED)
@Service
public abstract class DigitoUnicoService extends AbstractService<DigitoUnico, DigitoUnicoDto, Long> {

    @Autowired
    private DigitoUnicoRepository digitoUnicoRepository;

    @Autowired
    private DigitoUnicoConverterUtil converter;

    @Autowired
    private UsuarioService usuarioService;

    protected ConverterUtil<DigitoUnico, DigitoUnicoDto> getConverterUtil() {
        return (ConverterUtil<DigitoUnico, DigitoUnicoDto>) this.converter;
    }

    public List<DigitoUnicoDto> getAllByUsuario(Long idUsuario) {
        List<DigitoUnico> entities = digitoUnicoRepository.getAllByUsuario(idUsuario);
        log.debug(">> findAllByUsuario [entities={}] ", entities);
        List<DigitoUnicoDto> dtos = entities.parallelStream().map(entity -> converter.convertToDTO(entity))
                .collect(Collectors.toList());
        log.debug("<< findAllByUsuario [dtos={}] ", dtos);
        return dtos;
    }

    public DigitoUnicoDto createDigito(ParametroDigitoDTO paramDto, Integer digitoUnico) {
        DigitoUnicoDto dto = DigitoUnicoDto.builder().digitoParametro(paramDto.getDigitoParametro())
                .multiplicador(paramDto.getConcatenacao()).digitoGerado(digitoUnico).idUsuario(paramDto.getIdUsuario())
                .build();
        DigitoUnicoDto dtoSalvo = new DigitoUnicoDto();
        if (validarDigito(dto)) {
            log.debug(" >> Criar dígito [dto={}] ", dto);
            dtoSalvo = save(dto);
            log.debug(" << Criar dígito [dto={}] ", dto);
        }

        return dtoSalvo;
    }

    public Integer calcularDigitoUnico(ParametroDigitoDTO dto) {
        ResponseEntity<String> response = ValidatorCodigoUtil.validarParametros(dto);
        if (response.getStatusCode().equals(HttpStatus.BAD_REQUEST)) {
            throw new ResponseStatusException(response.getStatusCode(), response.getBody());
        }
        log.debug(" >> Calcular dígito único [dto={}] ", dto);
        Integer digitoUnico = calcular(dto);
        log.debug(" << Calcular dígito único [dto={}, Dígito único={}] ", dto, digitoUnico);

        return digitoUnico;
    }

    private Integer calcular(ParametroDigitoDTO paramDto) {
        Integer digitoUnico = CacheUtil.buscar(paramDto.getDigitoParametro(), paramDto.getConcatenacao());
        if (Objects.isNull(digitoUnico)) {
            digitoUnico = DigitoUtil.calcular(paramDto.getDigitoParametro(), paramDto.getConcatenacao());
            CacheUtil.adicionar(paramDto.getDigitoParametro(), paramDto.getConcatenacao(), digitoUnico);
        }
        return digitoUnico;
    }

    public boolean validarDigito(DigitoUnicoDto digitoUnico) {
        if (Objects.isNull(digitoUnico)) {
            return Boolean.FALSE;
        }
        log.debug(">> validarDigito [id={}] ", digitoUnico.getDigitoGerado());
        boolean result = Objects.nonNull(digitoUnico.getDigitoGerado()) && digitoUnicoRepository.validarDigito(digitoUnico.getDigitoGerado()) == 0;
        log.debug(">> validarDigito [id={}] ", digitoUnico.getDigitoGerado());
        return result;
    }

    public void validarUsuario(Long idUsuario) {
        boolean usuarioExists = usuarioService.validarUsuario(idUsuario);
        if (!usuarioExists) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "O usuário não existe!");
        }
    }

}

