package com.desafio.digitounico.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author: Josesmar Santos 14/09/2021
 * */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ParametroDigitoDTO {

    private Long idUsuario;
    private String digitoParametro;
    private Integer concatenacao;
}
