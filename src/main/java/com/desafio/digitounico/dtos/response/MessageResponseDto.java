package com.desafio.digitounico.dtos.response;

import lombok.Builder;
import lombok.Data;

/**
 * @autor: Josesmar Santos 14/09/2021
 * Resposta padrão para todos os métodos HTTP
 */

@Data
@Builder
public class MessageResponseDto {
    private String message;
}
