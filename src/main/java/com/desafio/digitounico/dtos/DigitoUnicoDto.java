package com.desafio.digitounico.dtos;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;

/**
 * @author: Josesmar Santos 14/09/2021
 * */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DigitoUnicoDto extends BaseDTO {

    private Long id;
    @NotNull
    @Min(1)
    private String digitoParametro;
    @Min(1)
    private Integer multiplicador;
    private Integer digitoGerado;
    @Min(1)
    private Long idUsuario;

}

