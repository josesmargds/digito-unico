package com.desafio.digitounico.dtos;

import com.desafio.digitounico.entities.DigitoUnico;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @author: Josesmar Santos 13/09/2021
 * */

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UsuarioDto extends BaseDTO {

    private Long id;

    @NotEmpty(message = "Nome deve ser informado.")
    @Length(min = 4, max = 255, message = "Nome deve ter entre 3 a 255 caracteres.")
    private String nome;

    @NotEmpty(message = "Email deve ser informado.")
    @Length(min = 8, max = 255, message = "Email deve ter entre 5 a 255 caracateres.")
    @Email(message = "Email informado inválido.")
    private String email;

    @JsonProperty("digitosUnicosCalculados")
    private List<DigitoUnicoDto> listDigitoUnico;
}
