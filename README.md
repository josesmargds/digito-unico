Desafio Dígito Único

Uma API que recebe um número inteiro e calcula um dígito único desse número. 

A função digitoUnico tem os seguintes parâmetros;
1. n: uma string representado um inteiro. 1<=n<=10ˆ1000000
2. k: um inteiro representando o número de vezes da concatenação
1<=k<=10ˆ5
3. A função digitoUnico deverá obrigatoriamente retornar um
inteiro.

Exemplo:

Se x tem apenas um dígito, então o seu dígito único é x.
Caso contrário, o dígito único de x é igual ao dígito único da
soma dos dígitos de x.

```
Por exemplo, o dígito único de 9875 será calculado como:
digito_unico(9875)9+8+7+5=29
digito_unico(29)2+9=11
```

O projeto conta também com endpoints para CRUD de Usuário e Dígitos Unicos.

### Tecnologias utilizadas

- Java 8
- Maven
- SpringBoot
- H2 Database

### Ferramentas

- Git (Controle de versão)
- Postman (Teste de endpoints)
- Swagger (Geração de documentação)
- Lombok (Produtividade e redução de código)

Teste
Para testar a aplicação, executar a aplicação de testes disponível em

```
src > test > java > DigitounicoApplicationTests
```
Instruções para a execução
o projeto conta com uma classe "DigitounicoApplication dentro do módulo de API para startar a aplicação através do SpringInitializer.

```
digito-unico > com.desafio.digitounico > DigitounicoApplication > Run as SpringBoot
```
ou por linha de comando

```bash
java -jar digito-unico.jar
```

### Paths e endpoints

Path original da API:  [Desafio Único API](http://localhost:8080/api)

Endpoints: [Swagger UI](http://localhost:8080/swagger-ui.html)



